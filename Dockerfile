FROM ubuntu:22.04

RUN apt update && apt install -y \
    python3-pip 

RUN mkdir /app
COPY ./hello-world.py /app

CMD python3 /app/hello-world.py